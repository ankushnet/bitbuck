package com.example.ankush.myapplication;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    EditText etuname, etpass;
    Button btnlogin, btnregister;
    AlertDialog.Builder builder;
    pref pref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        etuname = findViewById(R.id.etuname);
        etpass = findViewById(R.id.etpass);
        btnlogin = findViewById(R.id.btnlogin);
        btnregister = findViewById(R.id.btnregister);
        btnlogin.setOnClickListener(this);
        btnregister.setOnClickListener(this);
        pref=new pref(this);
        builder = new AlertDialog.Builder(this);

    }

    @Override
    public void onClick(View view) {
        int v = view.getId();
        switch (v) {
            case R.id.btnlogin: {
                pref.setIsLogin(true);
                dologin();
                break;
            }
            case R.id.btnregister: {
                Intent i = new Intent(MainActivity.this, register.class);
                startActivity(i);
                break;
            }

        }


    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();

        builder.setMessage(R.string.dialog_message).setTitle(R.string.dialog_title);

        //Setting message manually and performing action on button click
        builder.setMessage("Do you want to close this application ?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        finish();
                        Toast.makeText(getApplicationContext(), "Closing app",
                                Toast.LENGTH_SHORT).show();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //  Action for 'NO' Button
                        dialog.cancel();

                    }
                });
        //Creating dialog box
        AlertDialog alert = builder.create();
        //Setting the title manually
        alert.setTitle("Login Page");
        alert.show();

    }

    private void dologin() {
        String username = etuname.getText().toString().trim();
        String password = etpass.getText().toString().trim();
        if (username.isEmpty()) {
            etuname.setError("Enter username");
            return;

        }
        if (password.isEmpty()) {
            etpass.setError("Enter password");
            return;
        }
        if (username.equals("ankush") && password.equals("1235")) {
            Intent i = new Intent(MainActivity.this, listvieww.class);
            startActivity(i);
            return;

        } else {
            util.toast(this, "Enter valid username and password");

        }

    }


}

