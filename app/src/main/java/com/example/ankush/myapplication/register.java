package com.example.ankush.myapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class register extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemSelectedListener {
    EditText etname,etuuname,etmail,etpaass;
    CheckBox cbmusic,cbgames,cbdance;

    Spinner course;
    RadioButton male,female;
    Button btnregister;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        etname=findViewById(R.id.etname);
        etuuname=findViewById(R.id.etuuname);
        etmail=findViewById(R.id.etmail);
        etpaass=findViewById(R.id.etpaass);
        btnregister=findViewById(R.id.btnregister);
        btnregister.setOnClickListener(this);
        course=findViewById(R.id.interests);
        course.setOnItemSelectedListener(this);
        cbmusic=findViewById(R.id.cbmusic);
        cbgames=findViewById(R.id.cbgames);
        cbdance=findViewById(R.id.cbdance);

        // Spinner Drop down elements
        List<String> categories = new ArrayList<String>();
        categories.add("Choose");
        categories.add("CSE");
        categories.add("ECE");
        categories.add("EE");
        categories.add("CE");
        categories.add("ME");

        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, categories);

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        course.setAdapter(dataAdapter);
    }



    @Override
    public void onClick(View view) {
        int v=view.getId();
        switch (v)
        {
            case R.id.btnregister:
            {
                doregister();
                break;
            }
            case R.id.btnlogin:
            {
                break;
            }
        }

    }
    private void doregister()
    {
        String firstname=etname.getText().toString().trim();
        String username=etuuname.getText().toString().trim();
        String email=etmail.getText().toString().trim();
        String password=etpaass.getText().toString().trim();
        String Hobbies =hobbieschecked();
        if(firstname.isEmpty())
        {
            etname.setError("Enter first name");
            return;
        }
        if(username.isEmpty())
        {
            etuuname.setError("Enter username");
            return;
        }

        if(!Patterns.EMAIL_ADDRESS.matcher(email).matches())
        {
            etmail.setError("Enter valid email");
            return;
        }
        if(password.isEmpty())
        {
            etpaass.setError("Enter password");
            return;
        }
        if (course.getSelectedItem().toString().trim().equals("Choose")) {
            util.toast(this,"Select valid course");
            return;
        }
        if(Hobbies == "")
        {
            util.toast(this,"select hobbies");
            return;
        }

        util.toast(this,"User: " + username + ": registered");
        Intent i =new Intent(register.this,MainActivity.class);
        startActivity(i);

    }


    private String hobbieschecked()
    {
        String hobbies="" ;
        if (cbmusic.isChecked())
        {
            hobbies += "music";
        }
        if (cbgames.isChecked())
        {
            hobbies += "games";
        }
        if (cbdance.isChecked())
        {
            hobbies +="dance";
        }
        return hobbies;

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {


    }
}
