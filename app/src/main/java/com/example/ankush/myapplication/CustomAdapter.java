package com.example.ankush.myapplication;

import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class CustomAdapter extends BaseAdapter {
    Context context;
    ArrayList<String> list;
        ArrayList<Integer> img;
        Dialog dialog;

public CustomAdapter(listvieww listview,ArrayList<String> list,ArrayList<Integer> img)
{
    context=listview;
    this.list=list;
    this.img=img;
}
    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View vie = LayoutInflater.from(context).inflate(R.layout.mylist,parent,false);
        final TextView data;
        final ImageView imgw;
        data=vie.findViewById(R.id.tvdata);
        imgw=vie.findViewById(R.id.imgw);
        data.setText(list.get(position));
        imgw.setImageResource(img.get(position));
        imgw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int a;
                switch (position)
                {
                    case 0:
                        a=0;
                        zoomImage(a);
                        break;
                    case 1:
                        a=1;
                        zoomImage(a);
                        break;
                    case 2:
                        a=2;
                        zoomImage(a);
                        break;
                    case 3:
                        a=3;
                        zoomImage(a);
                        break;
                    case 4:
                        a=4;
                        zoomImage(a);
                        break;
                    case 5:
                        a=5;
                        zoomImage(a);
                        break;
                    case 6:
                        a=6;
                        zoomImage(a);
                        break;
                    case 7:
                        a=7;
                        zoomImage(a);
                        break;
                    case 8:
                        a=8;
                        zoomImage(a);
                        break;
                }
            }
        });
//        data.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                switch (position)
//                {
//                    case 0:
//                        util.toast(context,"AJ STYLES");
//                    case 1:
//                        util.toast(context,"DEAN AMBROSE");
//                    case 2:
//                        util.toast(context,"FINN BALOR");
//                    case 3:
//                        util.toast(context,"JOHN CENA");
//                    case 4:
//                        util.toast(context,"UNDERTAKER");
//                }
//            }
//        });
        return vie;

    }

    public void zoomImage(int a) {

        dialog = new Dialog(context);
        dialog.setContentView(R.layout.imagebox);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        ImageView iv;
        TextView tvName;
        Button btnOK;
        tvName=dialog.findViewById(R.id.tvName);
        iv = dialog.findViewById(R.id.ivShow);
        btnOK=dialog.findViewById(R.id.btnOk);
        iv.setImageResource(img.get(a));
        tvName.setText(list.get(a));
        btnOK.setOnClickListener(
                new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();

    }

}
