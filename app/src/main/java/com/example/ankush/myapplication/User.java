package com.example.ankush.myapplication;

public class User {
    private String name;
    private String userName;
    private String password;
    private String email;
    private String gender;
    private String hobbies;
    private String course;

    public User(String name, String userName, String password, String email, String gender, String hobbies, String course) {
        this.name=name;
        this.userName=userName;
        this.password=password;
        this.email=email;
        this.gender=gender;
        this.hobbies=hobbies;
        this.course=course;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }



    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getHobbies() {
        return hobbies;
    }

    public void setHobies(String hobies) {
        this.hobbies = hobbies;
    }

    public String getCourse() {
        return course;
    }

    public void setCourse(String course) {
        this.course = course;
    }


}