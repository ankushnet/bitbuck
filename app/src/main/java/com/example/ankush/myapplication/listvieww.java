package com.example.ankush.myapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class listvieww extends AppCompatActivity {
    ListView listView;
    GridView gridView;
    Button signout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listvieww);
        gridView=findViewById(R.id.gridview);
        signout=findViewById(R.id.signout);
        signout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pref pref=new pref(listvieww.this);
                pref.setIsLogin(false);
                startActivity(new Intent(listvieww.this,MainActivity.class));
                finish();
            }
        });

       // listView = (ListView) findViewById(R.id.listView);
        ArrayList<String> list = new ArrayList<>();
        list.add("AJ STYLES");
        list.add("DEAN AMBROSE");
        list.add("FINN BALOR");
        list.add("JOHN CENA");
        list.add("UNDERTAKER");
        list.add("REY MYSTERIO");
        list.add("TRIPLE H");
        list.add("RANDY ORTON");
        list.add("SHAWN MICHAELS");



        ArrayList<Integer> img =new ArrayList<>();
        img.add(R.drawable.aj);
        img.add(R.drawable.dean);
        img.add(R.drawable.finn);
        img.add(R.drawable.john);
        img.add(R.drawable.under);
        img.add(R.drawable.rey);
        img.add(R.drawable.hhh);
        img.add(R.drawable.orton);
        img.add(R.drawable.shawn);
        CustomAdapter adapter = new CustomAdapter(this, list,img);
        gridView.setAdapter(adapter);
        //listView.setAdapter(adapter);
    }}


