package com.example.ankush.myapplication;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class splash extends AppCompatActivity {
    private static int SPLASH_TIME_OUT = 3000;
    pref pref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        pref=new pref(this);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if(pref.getIsLogin())
                {
                    startActivity(new Intent(splash.this,listvieww.class));
                }
                else
                {
                    startActivity(new Intent(splash.this,MainActivity.class));
                }
              finish();
            }
        },SPLASH_TIME_OUT);
    }
}
