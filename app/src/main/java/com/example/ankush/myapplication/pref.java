package com.example.ankush.myapplication;

import android.content.Context;
import android.content.SharedPreferences;

public class pref {
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    public String key = "IsLogin";

    public pref(Context context) {
        sharedPreferences = context.getSharedPreferences(context.getPackageName(), Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
    }

    public void setIsLogin(Boolean status) {
        editor.putBoolean(key, status);
        editor.commit();
        editor.apply();
    }

    public boolean getIsLogin() {
        return sharedPreferences.getBoolean(key, false);
    }
}

